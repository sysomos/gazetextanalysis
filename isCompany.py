# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


'''
This program takes as input a companyName, reads the positive and negative jsons of that company, filters out the
"top_words" field and the "tags" field (if "use_tags" is set to 1), extracts features (words that occur in at least two
documents in either the positive or the negative class), splits the data randomly into 70/30 training and testing sets,
fits a decision tree and a delta tfidf model to the training set, predicts the testing set using the two models obtained,
and reports the final error. It also performs a leave-one-out cross-validation for both the models and reports those
errors too.
'''


import sys
import json
import math
import random
import itertools
import pprint
from collections import Counter
from sklearn import tree
from sklearn.cross_validation import train_test_split




def vectorize_documents(documents, feature_values):
	'''
	This function takes documents and turns each of them into a vector of length the size of "feature_values", where for
	each feature in "feature_values" the corresponding value in the vector of the document is the frequency of the feature
	in the document multiplied by the score of the feature from the list "feature_values", and returns a list of all such
	document vectors.

	:param documents: A list of list of words.
	:param feature_values: A list of scores corresponding to each feature.
	:return: A list of all document vectors.
	'''

	documents_vector = []

	for document in documents:
		document_vector = []
		frequency_counts = Counter(document)
		for feature in feature_values:
			if feature in frequency_counts:
				document_vector.append(frequency_counts.get(feature) * feature_values[feature])
			else:
				document_vector.append(0)
		documents_vector.append(document_vector)

	return documents_vector


def predict_deltatfidf(documents, term_idf):
	'''
	This function scores a given list of documents with a given list "term_idf" of scores of features, and returns a
	list of the scores for all the documents.

	:param documents: A list of list of words.
	:param term_idf: A list of (delta tfidf) scores corresponding to each feature.
	:return: A list of scores corresponding to the given list of documents.
	'''

	V = []

	for document in documents:
		if document != []:
			val = 0.0
			length = 0

			frequency_counts = Counter(document)
			for term in frequency_counts:
				if term in term_idf:
					val += frequency_counts.get(term) * term_idf[term]
					length += 1

			if length > 0:
				val = val / (length * 1.0)

			V.append(val)
		else:
			V.append(None)

	return V


def term_DF(features, documents):
	'''
	This function takes in a list of features and a list of documents and for each document returns a vector of size the
	length of the list "features", where for each feature the corresponding entry in that vector contains the frequency
	of that feature in that document.

	:param features: A list of words.
	:param documents: A list of list of words.
	:return: A list of frequency-count vectors corresponding to the given list of documents.
	'''

	frequency_counts = []
	for document in documents:
		frequency_counts.append(Counter(document))

	document_counts = dict()
	for feature in features:
		document_counts[feature] = 0

	for entry in frequency_counts:
		for feature in entry:
			if feature in features:
				document_counts[feature] += 1

	return document_counts


def term_IDF(features, data_pos, data_neg):
	'''
	This function takes in a list of features and data sets from the positive and negative classes and computes the
	difference in the frequency counts of each feature between the positive class and the negative class.

	:param features: A list of words.
	:param data_pos: A list of list of words.
	:param data_neg: A list of list of words.
	:return: A delta tfidf score corresponding to each feature.
	'''

	document_counts_pos = term_DF(features, data_pos)
	document_counts_neg = term_DF(features, data_neg)

	term_idf = dict()
	for feature in features:
		tmp = document_counts_pos[feature] - document_counts_neg[feature]
		if math.fabs(tmp) > 0:
			term_idf[feature] = tmp

	return term_idf


def term_log2_IDF(features, data_pos, data_neg):
	'''
	This function takes in a list of features and data sets from the positive and negative classes and computes the
	difference in the log of the frequency counts of each feature between the positive class and the negative class.

	:param features: A list of words.
	:param data_pos: A list of list of words.
	:param data_neg: A list of list of words.
	:return: A delta tfidf score corresponding to each feature.
	'''

	document_counts_pos = term_DF(features, data_pos)
	document_counts_neg = term_DF(features, data_neg)

	term_log2_idf = dict()
	for feature in features:
		tmp = math.log((document_counts_pos[feature] + 1.0) / (document_counts_neg[feature] + 1.0), 2)
		if math.fabs(tmp) > 0:
			term_log2_idf[feature] = tmp

	return term_log2_idf


def term_signed_IDF(features, data_pos, data_neg):
	'''
	This function takes in a list of features and data sets from the positive and negative classes and computes the
	difference in the frequency counts of each feature between the positive class and the negative class, and returns 1
	if the difference is positive, and -1 if the difference is negative.

	:param features: A list of words.
	:param data_pos: A list of list of words.
	:param data_neg: A list of list of words.
	:return: A delta tfidf score corresponding to each feature.
	'''

	term_signed_idf = term_IDF(features, data_pos, data_neg)

	for feature in term_signed_idf:
		if term_signed_idf[feature] < 0:
			term_signed_idf[feature] = -1
		elif term_signed_idf[feature] > 0:
			term_signed_idf[feature] = 1

	return term_signed_idf


def decision_tree(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test):
	'''
	This function first computes the feature values (basically the IDF scores) of the features, then fits a decision
	tree model to the training data, predicts it on the testing data, and finally returns the test error as a percentage.

	:param features: A list of words.
	:param data_pos_train: A list of list of words corresponding to the positive training examples.
	:param data_neg_train: A list of list of words corresponding to the negative training examples.
	:param data_pos_test: A list of list of words corresponding to the positive testing examples.
	:param data_neg_test: A list of list of words corresponding to the negative testing examples.
	:return: Prediction error percentage.
	'''

	feature_values = term_IDF(features, data_pos_train, data_neg_train)

	data_pos_train_vec = vectorize_documents(data_pos_train, feature_values)
	target_pos_train_vec = [1] * len(data_pos_train_vec)
	data_neg_train_vec = vectorize_documents(data_neg_train, feature_values)
	target_neg_train_vec = [0] * len(data_neg_train_vec)
	data_train_vec = data_pos_train_vec + data_neg_train_vec
	target_train_vec = target_pos_train_vec + target_neg_train_vec

	clf = tree.DecisionTreeClassifier()
	clf = clf.fit(data_train_vec, target_train_vec)

	data_pos_test_vec = vectorize_documents(data_pos_test, feature_values)
	target_pos_test_vec = [1] * len(data_pos_test_vec)
	data_neg_test_vec = vectorize_documents(data_neg_test, feature_values)
	target_neg_test_vec = [0] * len(data_neg_test_vec)
	data_test_vec = data_pos_test_vec + data_neg_test_vec
	target_test_vec = target_pos_test_vec + target_neg_test_vec

	predicted_test_vec = clf.predict(data_test_vec)

	error = 0
	for i in xrange(len(target_test_vec)):
		if target_test_vec[i] != predicted_test_vec[i]:
			error += 1
			#if i < len(data_pos_test):
			#	print data_pos_test[i]
			#else:
			#	print data_neg_test[i - len(data_pos_test)]
			#print str(clf.predict_proba(data_test_vec[i])) + str(' : ') + str(target_test_vec[i])
			#print '\n'

	error_percentage = error / (len(data_test_vec) * 1.0) * 100
	#print 'Error (in Decision Tree model) : ' + str(error_percentage) + '%\n'

	return error_percentage


def delta_tfidf(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test):
	'''
	This function first computes the feature values (basically the IDF scores) of the features, then fits a delta tfidf
	model to the training data, predicts it on the testing data, and finally returns the test error as a percentage.

	:param features: A list of words.
	:param data_pos_train: A list of list of words corresponding to the positive training examples.
	:param data_neg_train: A list of list of words corresponding to the negative training examples.
	:param data_pos_test: A list of list of words corresponding to the positive testing examples.
	:param data_neg_test: A list of list of words corresponding to the negative testing examples.
	:return: Prediction error percentage.
	'''

	feature_values = term_IDF(features, data_pos_train, data_neg_train)

	data_test_vec = data_pos_test + data_neg_test
	target_test_vec = [1 for item in data_pos_test] + [-1 for item in data_neg_test]

	predicted_test_vec = predict_deltatfidf(data_test_vec, feature_values)

	error = 0
	for i in xrange(len(target_test_vec)):
		if target_test_vec[i] * predicted_test_vec[i] < 0:
			error += 1
			#if i < len(data_pos_test):
			#	print data_pos_test[i]
			#else:
			#	print data_neg_test[i - len(data_pos_test)]
			#print str(predicted_test_vec[i]) + str(' : ') + str(target_test_vec[i])
			#print '\n'

	error_percentage = error / ((len(data_test_vec)) * 1.0) * 100
	#print 'Error (in Delta-TfIdf model) : ' + str(error_percentage) + '%\n'

	return error_percentage


def feature_selection(data_pos, data_neg):
	'''
	This function reads the documents in "data_pos" and "data_neg" and picks out words that have occurred in more than
	one document either in the positive class or in the negative class.

	:param data_pos: A list of list of words corresponding to the positive class.
	:param data_neg: A list of list of words corresponding to the negative class.
	:return: A set of words that are frequent in at least one class.
	'''

	corpus = data_pos + data_neg
	features = list(set(itertools.chain(*corpus)))

	document_counts_pos = term_DF(features, data_pos)
	document_counts_neg = term_DF(features, data_neg)
	document_counts_pos = {key:value for key, value in document_counts_pos.items() if value >= 2}
	document_counts_neg = {key:value for key, value in document_counts_neg.items() if value >= 2}

	features = set(list(document_counts_pos) + list(document_counts_neg))
	#print 'Number of features : ' + str(len(features)) + '\n'

	return features


def read_json_data(companyName, use_tags):
	'''
	This function reads the positive and negative json files corresponding to the name of the company specified by
	"companyName". It parses out the 'top_words' field, and if the boolean "use_tags" is set to true, it also parses out
	the "tags" field. It combines the results into "data_pos_topWords" for the positive class and "data_neg_topWords" for
	the negative class, and returns a list of these two lists.

	:param companyName: A string containing the name of the company whose json is to be parsed.
	:param use_tags: A Boolean deciding whether to use "tags" or not.
	:return: A list of lists containing the keywords for the positive class and the negative class, respectively.
	'''

	data_pos = json.load(open('out/' + companyName + '_pos.json', 'r'))
	data_neg = json.load(open('out/' + companyName + '_neg.json', 'r'))
	data_pos = [entry for entry in data_pos if entry['lang'] == 'en']
	data_neg = [entry for entry in data_neg if entry['lang'] == 'en']

	#pprint.pprint(data_pos[0])
	#print '\n'

	if use_tags:
		data_pos_topWords = [data['top_words'] + data['metadata']['tags'] for data in data_pos]
		data_neg_topWords = [data['top_words'] + data['metadata']['tags'] for data in data_neg]
	else:
		data_pos_topWords = [data['top_words'] for data in data_pos]
		data_neg_topWords = [data['top_words'] for data in data_neg]

	data_pos_topWords = [data for data in data_pos_topWords if data != []]
	data_neg_topWords = [data for data in data_neg_topWords if data != []]

	return [data_pos_topWords, data_neg_topWords]


def main(companyName, use_tags):
	'''
	This is the main function. It reads the jsons corresponding to the positive class and the negative class for the
	given "companyName". It splits the datasets randomly into 70/30 training and testing datasets, and fits both the
	models decision_tree and delta_tfidf to the training data and computes the errors on the testing data. Then it also
	performs a leave-one-out cross-validation for both the models and reports their errors as well.

	:param companyName: A string containing the name of the company whose json is to be parsed.
	:param use_tags: A Boolean deciding whether to use "tags" or not.
	'''

	[data_pos, data_neg] = read_json_data(companyName, use_tags)

	print 'Number of documents in the positive class : ' + str(len(data_pos))
	print 'Number of documents in the negative class : ' + str(len(data_neg))
	print '\n'

	print 'Random 70/30 partition error'
	error_percentage_decision_tree = 0
	error_percentage_delta_tfidf = 0
	number_of_simulations = 100
	for i in xrange(number_of_simulations):
		random_state = random.randint(1, 10000)
		[data_pos_train, data_pos_test] = train_test_split(data_pos, test_size=0.3, random_state=random_state)
		[data_neg_train, data_neg_test] = train_test_split(data_neg, test_size=0.3, random_state=random_state)

		#print 'Number of documents in the training set : ' + str(len(data_pos_train) + len(data_neg_train))
		#print 'Number of documents in the testing set : ' + str(len(data_pos_test) + len(data_neg_test))
		#print '\n'

		features = feature_selection(data_pos_train, data_neg_train)

		error_percentage_decision_tree += decision_tree(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test)
		error_percentage_delta_tfidf += delta_tfidf(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test)

	print 'Decision Tree : ' + str(round(error_percentage_decision_tree / (number_of_simulations * 1.0), 2)) + ' %'
	print 'Delta TfIdf   : ' + str(round(error_percentage_delta_tfidf / (number_of_simulations * 1.0), 2)) + ' %\n'

	print 'Leave-one-out cross-validation error'
	error_percentage_decision_tree = 0
	error_percentage_delta_tfidf = 0
	number_of_simulations = len(data_pos)
	for doc1, doc2 in zip(data_pos, data_neg):
		data_pos_train = [doc for doc in data_pos if doc != doc1]
		data_pos_test = [doc1]
		data_neg_train = [doc for doc in data_neg if doc != doc2]
		data_neg_test = [doc2]

		features = feature_selection(data_pos_train, data_neg_train)

		error_percentage_decision_tree += decision_tree(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test)
		error_percentage_delta_tfidf += delta_tfidf(features, data_pos_train, data_neg_train, data_pos_test, data_neg_test)

	print 'Decision Tree : ' + str(round(error_percentage_decision_tree / (number_of_simulations * 1.0), 2)) + ' %'
	print 'Delta TfIdf   : ' + str(round(error_percentage_delta_tfidf / (number_of_simulations * 1.0), 2)) + ' %\n'



if __name__ == '__main__':
	use_tags = 0
	main(sys.argv[1], use_tags)