# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'
__author__ = 'Maruti Agarwal'


'''
This program takes as input a brandName, reads the positive and negative jsons of that brand, filters out the
"top_words" field and the "tags" field (if "use_tags" is set to 1), extracts features (words that occur in at least two
documents in either the positive or the negative class), splits the data randomly into 70/30 training and testing sets,
fits a delta tfidf model to the training set, predicts the testing set using the model obtained, and reports the final
error. It also uses the model to do prediction for new test data."
'''


import os
import json
import math
import itertools
from collections import Counter
import random
from sklearn.cross_validation import train_test_split
from cPickle import dump, load, HIGHEST_PROTOCOL


class Context():
	ID = "relevanttext"

	def __init__(self, DATA_DIR, useTags=False):
		self.USE_TAGS = useTags
		self.DATA_DIR = os.path.join(DATA_DIR, self.ID)
		if not os.path.exists(self.DATA_DIR):
			os.makedirs(self.DATA_DIR)


	def parse_json(self, json_data):
		'''
		This function reads the test json files corresponding to the name of the brand specified by "brandName". It
		parses out the 'top_words' field, and if the boolean "use_tags" is set to true, it also parses out the "tags"
		field. It combines the results into "data_pos_topWords" for the positive class and "data_neg_topWords" for
		the negative class, and returns a list of these two lists.

		:param companyName: A string containing the name of the company whose json is to be parsed.
		:param use_tags: A Boolean deciding whether to use "tags" or not.
		:return: A list of lists containing the keywords for the positive class and the negative class, respectively.
		'''

		# data = #json containing fields - 'lang', 'top_words', 'metadata'
		# let's stick to ENGLISH language only for now, filter out non-english posts
		data = [entry for entry in json_data if entry['lang'] == 'en']

		if self.USE_TAGS:
			data_topWords = [entry['top_words'] + entry['metadata']['tags'] for entry in data]
		else:
			data_topWords = [entry['top_words'] for entry in data]

		data_topWords = [entry for entry in data_topWords if entry != []]

		return data_topWords


	def getTrainingData(self, brandName):
		'''
		This function reads the positive and negative json files corresponding to the name of the brand specified by
		"brandName". It parses out the 'top_words' field, and if the boolean "SELF.USE_TAGS" is set to true, it also
		parses out the "tags" field. It combines the results into "data_pos_topWords" for the positive class and
		"data_neg_topWords" for the negative class, and returns a list of these two lists.

		:param brandName: A string containing the name of the brand whose json is to be parsed.
		:return: A list of lists containing the keywords for the positive class and the negative class, respectively.
		'''

		data_pos = json.load(open('out/' + brandName + '_pos.json', 'r'))
		data_neg = json.load(open('out/' + brandName + '_neg.json', 'r'))

		data_pos_topWords = self.parse_json(data_pos)
		data_neg_topWords = self.parse_json(data_neg)

		print 'Number of documents in the positive class : ' + str(len(data_pos_topWords))
		print 'Number of documents in the negative class : ' + str(len(data_neg_topWords))

		return [data_pos_topWords, data_neg_topWords]


	def splitTrainTestData(self, data_pos, data_neg, fraction=0.3):
		random_state = random.randint(1, 10000)
		[data_pos_train, data_pos_test] = train_test_split(data_pos, test_size=fraction, random_state=random_state)
		[data_neg_train, data_neg_test] = train_test_split(data_neg, test_size=fraction, random_state=random_state)
		return [data_pos_train, data_pos_test, data_neg_train, data_neg_test]


	def term_DF(self, features, documents):
		'''
		This function takes in a list of features and a list of documents and for each document returns a vector of size the
		length of the list "features", where for each feature the corresponding entry in that vector contains the frequency
		of that feature in that document.

		:param features: A list of words.
		:param documents: A list of list of words.
		:return: A list of frequency-count vectors corresponding to the given list of documents.
		'''

		frequency_counts = []
		for document in documents:
			frequency_counts.append(Counter(document))

		document_counts = dict()
		for feature in features:
			document_counts[feature] = 0

		for entry in frequency_counts:
			for feature in entry:
				if feature in features:
					document_counts[feature] += 1

		return document_counts


	def term_DF_selection(self, data_pos, data_neg):
		'''
		This function reads the documents in "data_pos" and "data_neg" and picks out words that have occurred in more than
		one document either in the positive class or in the negative class.

		:param data_pos: A list of list of words corresponding to the positive class.
		:param data_neg: A list of list of words corresponding to the negative class.
		:return: A set of words that are frequent in at least one class.
		'''

		corpus = data_pos + data_neg # A list of list of words
		features = list(set(itertools.chain(*corpus))) # A list of words

		document_counts_pos = self.term_DF(features, data_pos) # A dict of words, with their frequency
		document_counts_neg = self.term_DF(features, data_neg)

		# filter out words with frequency less than 2
		document_counts_pos = {key:value for key, value in document_counts_pos.items() if value >= 2}
		document_counts_neg = {key:value for key, value in document_counts_neg.items() if value >= 2}

		# a set of high-frequency words
		features = set(list(document_counts_pos) + list(document_counts_neg))
		#print 'Number of features : ' + str(len(features)) + '\n'

		return features


	def term_IDF(self, features, data_pos, data_neg):
		'''
		This function takes in a list of features and data sets from the positive and negative classes and computes the
		difference in the frequency counts of each feature between the positive class and the negative class.

		:param features: A list of words.
		:param data_pos: A list of list of words.
		:param data_neg: A list of list of words.
		:return: A delta tfidf score corresponding to each feature.
		'''

		document_counts_pos = self.term_DF(features, data_pos)
		document_counts_neg = self.term_DF(features, data_neg)

		term_idf = dict()
		for feature in features:
			tmp = document_counts_pos[feature] - document_counts_neg[feature]
			if math.fabs(tmp) > 0:
				term_idf[feature] = tmp

		return term_idf


	def predict_deltatfidf(self, documents, term_idf):
		'''
		This function scores a given list of documents with a given list "term_idf" of scores of features, and returns a
		list of the scores for all the documents.

		:param documents: A list of list of words.
		:param term_idf: A list of (delta tfidf) scores corresponding to each feature.
		:return: A list of scores corresponding to the given list of documents.
		'''

		V = []
		print 'predict_deltatfidf: len(documents) = ', len(documents)
		for document in documents:
			if document != []:
				val = 0.0
				length = 0

				frequency_counts = Counter(document)
				for term in frequency_counts:
					if term in term_idf:
						val += frequency_counts.get(term) * term_idf[term]
						length += 1

				if length > 0:
					val = val / (length * 1.0)

				V.append(val)
			else:
				V.append(None)

		return V


	def compute_error(self, feature_values, data_pos_test, data_neg_test):
		'''
		This function first computes the feature values (basically the IDF scores) of the features, then fits a delta tfidf
		model to the training data, predicts it on the testing data, and finally returns the test error as a percentage.

		:param features: A list of features along with their scores.
		:param data_pos_test: A list of list of words corresponding to the positive testing examples.
		:param data_neg_test: A list of list of words corresponding to the negative testing examples.
		:return: Prediction error percentage.
		'''

		data_test_vec = data_pos_test + data_neg_test
		target_test_vec = [1 for item in data_pos_test] + [-1 for item in data_neg_test]

		predicted_test_vec = self.predict_deltatfidf(data_test_vec, feature_values)

		error = 0
		for i in xrange(len(target_test_vec)):
			if target_test_vec[i] * predicted_test_vec[i] < 0:
				error += 1
				#if i < len(data_pos_test):
				#	print data_pos_test[i]
				#else:
				#	print data_neg_test[i - len(data_pos_test)]
				#print str(predicted_test_vec[i]) + str(' : ') + str(target_test_vec[i])
				#print '\n'

		error_percentage = error / ((len(data_test_vec)) * 1.0) * 100
		return error_percentage


	def train(self, brandName):
		[data_pos, data_neg] = self.getTrainingData(brandName)
		[data_pos_train, data_pos_test, data_neg_train, data_neg_test] = self.splitTrainTestData(data_pos, data_neg)
		features_term_DF = self.term_DF_selection(data_pos_train, data_neg_train)
		features_term_IDF = self.term_IDF(features_term_DF, data_pos_train, data_neg_train)

		# save features_term_IDF to be used in scoring
		self.BRAND_FEATURES_IDF = os.path.join(self.DATA_DIR, brandName + '.pkl')
		dump(features_term_IDF, open(self.BRAND_FEATURES_IDF, 'wb'), HIGHEST_PROTOCOL)

		error_percentage = self.compute_error(features_term_IDF, data_pos_test, data_neg_test)
		print 'Delta TfIdf Percentage Error : ' + str(round(error_percentage, 2))


	def predict(self, brandName, features_term_IDF=None):
		json_data = json.load(open('out/testdata/' + brandName + '.json', 'r'))
		documents = self.parse_json(json_data)

		# load term_IDF features
		if not features_term_IDF:
			self.BRAND_FEATURES_IDF = os.path.join(self.DATA_DIR, brandName + '.pkl')
			features_term_IDF = load(open(self.BRAND_FEATURES_IDF, 'rb'))

		# get score for each document
		print 'document_topWords = ', documents
		scores = self.predict_deltatfidf(documents, features_term_IDF)
		return scores



if __name__ == '__main__':
	textcontext = Context(DATA_DIR='out')
	textcontext.train('apple')
	print textcontext.predict('apple')